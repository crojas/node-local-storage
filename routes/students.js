var express = require('express');
var storage = require('node-persist');
var router = express.Router();

var store_key = 'students';

storage.initSync();

router.get('/', function(req, res) {
    if (storage.getItem(store_key)) {
        res.status('200').json(storage.getItem(store_key));
    }else{
        res.status('200').json({});
    }
});

router.post('/', function(req, res) {

    var students = storage.getItem(store_key);

    if(!students){
        students = [];
    }

    students.push(req.body.student);

    storage.setItem(store_key, students);

    res.status('201').json({status: "Created"});
});

var update_student  = function(original, updated) {
	var students = storage.getItem(store_key);
	original.first_name = updated.first_name;
	original.last_name = updated.last_name;
    storage.setItem(store_key, students);
}

var find_student_by_id = function(id) {
	var students = storage.getItem(store_key);
	return students.find(function(element) {
        return element.id == id;
    });
}

router.put('/:id', function(req, res) {
	var student = find_student_by_id(req.params.id);
	if(!student){
		return res.status('404').json({});
	}
	update_student(student, req.body.student);
    res.status('204').json({});
});

router.patch('/:id', function(req, res) {
	var student = find_student_by_id(req.params.id);
	if(!student){
		return res.status('404').json({});
	}
	update_student(student, req.body.student);
    res.status('204').json({});
});

router.delete('/:id', function(req, res) {
	var student = find_student_by_id(req.params.id);
	if(!student){
		return res.status('404').json({});
	}
	var students = storage.getItem(store_key) || [];
	storage.setItem(store_key, students.filter(function(item, idx) {return !(item.id == req.params.id) }));
    res.status('204').json({});
});

module.exports = router;
