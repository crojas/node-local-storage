var express = require('express');
var storage = require('node-persist');
var router = express.Router();

var store_key = 'notes';

storage.initSync();

router.get('/', function(req, res) {
    var notes = storage.getItem(store_key) || [];
    if (notes) {
        res.status('200').json(notes);
    }
});

var find_note_by_student_id = function(id) {
  var notes = storage.getItem(store_key) || [];
  return notes.find(function(element) {
    return element.student_id == id;
  });
}

router.get('/:id', function(req, res) {
    var note = find_note_by_student_id(req.params.id);
    if (note) {
        res.status('200').json(note);
    }else{
        res.status('404').json({});
    }
});

router.post('/:id', function(req, res) {
    var notes = storage.getItem(store_key) || [];
    var note = find_note_by_student_id(req.params.id);
    if (note) {
      note.value = req.body.note.value;
    } else{
      notes.push({student_id: req.params.id, value: req.body.note.value});
    }
    storage.setItem(store_key, notes);
    res.status('201').json({status: "Created"});
});

router.put('/:id', function(req, res) {
    var notes = storage.getItem(store_key) || [];
    var note = find_note_by_student_id(req.params.id);
    if (note) {
      note.value = req.body.note.value;
      storage.setItem(store_key, notes);
    } else{
        res.status('404').json({});
    }
    res.status('204').json({status: "No Content"});
});

router.patch('/:id', function(req, res) {
    var notes = storage.getItem(store_key) || [];
    var note = find_note_by_student_id(req.params.id);
    if (note) {
      note.value = req.body.note.value;
      storage.setItem(store_key, notes);
    } else{
        res.status('404').json({});
    }
    res.status('204').json({status: "No Content"});
});

module.exports = router;
