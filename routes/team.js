var express = require('express');
var storage = require('node-persist');
var router = express.Router();

var store_key = 'team';

storage.initSync();

var find_member_by_id = function(id) {
    var team = storage.getItem(store_key) || {};
    var members = team.members;

    return members.find(function(member) {
        if(member.id == id){
            return member;
        }
    });

}

/* GET users listing. */
router.get('/', function(req, res) {
    if (storage.getItem(store_key)) {
        res.status('200').json(storage.getItem(store_key));
    }else{
        res.status('200').json({});
    }
});

router.get('/:member', function(req, res) {
    var member = find_member_by_id(req.params.member);
    if (member){
        res.status('200').json(member);
    }else{
        res.status('404').json({});
    }
});

module.exports = router;
